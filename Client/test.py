def comptage(cards):
    count = {"HEARTS":0,"SPADES":0,"DIAMONDS":0,"CLUBS":0}
    for c in cards:
        count[c["suit"]] += 1
    return count

def test_comptage():

    #  GIVEN
    cards = [{'value': '10', 'suit': 'HEARTS', 'code': '0H'},
            {'value': '5', 'suit': 'DIAMONDS', 'code': '5D'},
            {'value': '9', 'suit': 'HEARTS', 'code': '9H'},
            {'value': '10', 'suit': 'SPADES', 'code': '0S'},
            {'value': '6', 'suit': 'HEARTS', 'code': '6H'},
            {'value': 'ACE', 'suit': 'DIAMONDS', 'code': 'AD'},
            {'value': '4', 'suit': 'SPADES', 'code': '4S'},
            {'value': '1', 'suit': 'SPADES', 'code': '1H'},
            {'value': '3', 'suit': 'DIAMONDS', 'code': '3D'}]
    expected = {'HEARTS': 3, 'SPADES': 3, 'DIAMONDS': 3, 'CLUBS': 0}
    #  WHEN
    obtained = comptage(cards)
    # THEN
    assert expected == obtained

if __name__ == "__main__":
    test_comptage()
