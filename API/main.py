from fastapi import FastAPI

import requests

app = FastAPI()

id=""

@app.get("/creer-un-deck/")
def Créé_Deck():
    global id
    deck = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    deck = deck.json()
    id=deck["deck_id"]
    return  (id,"id ="+id)



@app.post("/cartes/{nombre_cartes}")
def Tirer_Cartes(nombre_cartes):
    global id
    tirages = requests.get("https://deckofcardsapi.com/api/deck/"+id+"/draw/?count="+nombre_cartes)
    tirages = tirages.json()
    cartes = tirages["cards"]
    if cartes == []:
        print("aucune carte dans le deck, veuillez en créer un autre")
        return
    id = tirages["deck_id"]
    remaining = tirages["remaining"]
    nombre = len(cartes)
    if remaining == 0:
        print("plus de carte dans le deck, seulement "+str(nombre)+" cartes ont était tiré")
    return {"cartes": cartes}


@app.get("/")
def readRoot():
    return{"Bienvenue sur notre jeu de cartes"}


