Bienvenue dans le TP noté de conception logiciel
Pour lancer le code :
- Clonez le dépot git
- Copiez les lignes de commandes se situant dans le fichier readme dans le dossier API dans un terminal
- Lancez un nouveau terminal et tapez les lignes de commandes du fichiers readme dans le dossier Client

Quickstart :
Dans un terminal :
`git init git clone https://gitlab.com/colinEN/examen-conception-logiciel.git cd examen-conception-logiciel/API pip install -r requirements.txt uvicorn main:app`
Dans un autre terminal
cd Client pip install -r requirements.txt python main.py 
